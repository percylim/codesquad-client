import React, { useState, useEffect, RadioButton, useRef } from 'react'
// mport validator from 'validator'
// import ReactDOM from 'react-dom';
import EscapeStr from './mysqlConvertChar';
//import { withRouter } from "react-router-dom";
import Axios from "axios";

 import  './UserProfile.css';

 require('dotenv').config();//
 const url = process.env.REACT_APP_SERVER_URL;
const fetch = require('node-fetch');
var lastSix = '';
//var department = '';
//var glType= '';
//var data =[];
var depData = [];
 var typeData = [];
var glData = [];
//var history = useHistory();
const companyID = localStorage.getItem('companyID');
var glNo = localStorage.getItem('glNo');
var glSub = localStorage.getItem('glSub');
//var companyName = localStorage.getItem('companyName')
//  var eDate = '';
//   var sDate = '';
 // alert(glNo+' = '+glSub);


  function glEdit () {
  const [depData, setDepDate] = useState([]);
  const [typeData, setTypeDate] = useState([]);
  const [glData, setGlData] = useState([]);
  const [glType, setGlType] = useState('');
  const [department, setDepartment] = useState('');
  const [glName, setGlName] = useState('');
  const [glDescription, setGlDescription] = useState('');

// ***************

const useEffect (() => {
glNo = localStorage.getItem('glNo');
 glSub = localStorage.getItem('glSub');

  const body = {
    companyID : companyID,
    glNo: glNo,
    glSub: glSub,
  };

 Axios({
  method: 'post',
  url: url+'/departmentInfo',
  data: body
})
.then(res => {
  console.log(res);
  setDepData(res.data);

  if (res.data.length === 0) {
  //  window.location.href ='glEdit';
  }
  department = res.data[0].department;
  
 
  // window.alert(data[1].description);
})
.catch(function (error) {
  alert("server connection Fail in Department");
  
  });


Axios({
  method: 'post',
  url: url+'/glTypeInfo',
  data: body
})
.then(res => {
  console.log(res);
  setTypeData(res.data);
 
 // setGlType(res.data[0].glType);
  
  // window.alert(data[1].description);
})
.catch(function (error) {
  alert("server connection Fail in G/L account Type");
  
  });

Axios({
  method: 'post',
  url: url+'/glData',
  data: body
})
.then(res => {
console.log(res);
setGlData(res.data);

if (glData.length === 0) {
//  window.location.href ='glEdit';
}
// alert(glData[0].glDescription);
 
 setDepartment(glData[0].department);
 
 
   setGlName(glData[0].glName);
   setGlDescription(glData[0].glDescription);
   setGlType(glData[0].glType)


// alert(glData[0].glName);

})

.catch(function (error) {
alert("server connection Fail in G/L Account Data");
// window.location.href ='glEdit';
});


});
const handleChangeDep = async (e) => {  
    e.preventDefault();
   let depNo = e.target.value;
 
   for (let i = 0; i < depData.length; i++) {
       if (depData[i].department === depNo) {
           setDepartment(depData[i].department);
       }


   }

 //  alert(depNo);
 }
 const handleChangeType = async (e) => { 
    e.preventDefault();
 //  alert(e);
  // this.setState({ glType: e.target.value });
   let gType = e.target.value;
   for (let i = 0; i < typeData.length; i++) {
  if (typeData[i].glType === gType) {
    setGlType(typeData[i].glType);
  }


}

 } ;



 const handleSubmit = async (e) => {
  
     // alert("#0");
      e.preventDefault();

      if(this.validate()){
        console.log(this.state);
       // alert(this.sexEl.current.value);

        const user= {
          companyID: EscapeStr(companyID),
          glNo: EscapeStr(glNo),
        //    glNo: '129930',
        glSub: EscapeStr(glSub),

          department: department,
          glName: EscapeStr(glNameEl),
          glDescription: EscapeStr(glDescription),

          glType: glType,

         };
         //var name1 =  EscapeStr(user.companyName);
      //  alert(this.dateBirthEl.current.value);
        fetch(url+'/glUpdate', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify( user )
          // We convert the React state to JSON and send it as the POST body
         // data: JSON.stringify(user,user.ame)
          }).then(function(response) {
           return response.text()
        }).then(function(text) {


          // alert(text);
         lastSix = text.substr(text.length - 7); // => "Tabs1"
          //  poemDisplay.textContent = text;
          // alert(lastSix);

           if (lastSix === 'Success') {
           // localStorage.clear();
           localStorage.removeItem("glNo");
           localStorage.removeItem("glSub");
            window.location.reload(false);
            window.location.href ='glList';
           };
          })
            .catch(error => {
            // alert(  error.toString() );
               alert('error occured :'+error);
             })

    }



  }

  
  const validate = async () => {
    
        if (glNo ==="") {
            alert("General Ledger No.Must not blank");
            return false;

         };

         if (glSub ==="") {
            alert("General Ledger Sub No. Must not blank");
            return false;
         };

         if (glName ==="") {
            alert("General Ledger Name Must not blank");
            return false;
         };
         if (department ==="") {
          alert("Department not selected");
          return false;
       };

       if (glDescription ==="") {
        alert("General Ledger Description Must not blank");
        return false;
     };

     if (glType ==="") {
      alert("General Ledger Account not selected");
      return false;
    };
     // let input = this.state.input;
           // let errors = {};
          //  let isValid = true;
        //   if (this.companyIDEl.current.value.length < 8){
        //   alert("Company ID must be from 8 - 20 character");
         //  return false;
         //  }



      return true;
    }
    const allowOnlyNumericsOrDigits = async (e) => {
 
		const charCode = e.which ? e.which : e.keyCode;

		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			alert('error: OOPs! Only numeric values or digits allowed' );
            return false;
		}
	};

//  handleChange(e) {
 //   this.setState({ value: e.target.value});
 // }

//handleBlur(e) {
//  var num = parseFloat(this.state.value);
//  var cleanNum = num.toFixed(2);
//  this.setState({value: cleanNum});
//}

const onValueChange = async (e) => {
//const onValueChange (e) => {
//  this.setState({value :e.target.value})
}


const handleInputChange = async (e) => {
 
    console.log(e.target.value)
  //  this.setState({
  //    number: event.target.value
  //  })
  }

  const formatInput = async (e) => {
    const num = e.target.value
   // this.setState({
  //      number: parseFloat(num).toFixed(2)
  //  })
  }


  const handleBack = async (e) => {
 
    window.location="/glList";
  };






  
        const mystyle = {
            color: "BLACK",
            backgroundColor: "#ffffff",
            padding: "5px 15px 10px 10px",
            alignItems: "left",
            fontFamily: "Arial",



        };
  //  const toInputLowercase = e => {
  //  e.target.value = ("" + e.target.value).toLowerCase();
  //  };
/*
         const buttonStyle = {
          color: "black",
          backgroundColor: "yellow",
          padding: "10px 15px 10px 10px",
          fontFamily: "Arial",
          position: 'absolute',
          right: 350,
      };
*/
   const subStyle = {
          color: "white",
          backgroundColor: "blue",
          padding: "10px 15px 10px 10px",
          fontFamily: "Arial",

      };
      const logstyle = {
          color: "white",
          backgroundColor: "red",
          padding: "2px 15px 10px 18px",
          fontFamily: "Arial",
          position: 'absolute',
          right: 800,
          width: '6em',
          height: '3em',

      };









      return (


        <form style={mystyle} onSubmit={this.handleSubmit}>
          <fieldset>

           <h1>Edit General Ledger Profile</h1>
           <label>G/L No. :
          <input class="text-uppercase" minlength={4} maxLength={4} ref={this.glNoEl}  name="glNo" readOnly= {true} required />
          </label>
           <label>G/L Sub No. :
          <input type="text-uppercase" minLength={3} maxLength={3} ref={this.glSubEl} name="glSub" readOnly= {true} required />
          </label>
          <label> G/L Name :
          <input type="text" maxLength={50} ref={this.glNameEl} name="glName" required />
          </label>

          <label>G/L Description :
          <input type="text" maxLength={50} ref={this.glDescriptionEl}  name="glDescription" required />
          </label>






          <div className="select-container">
          <label style={{paddingRight: '260px'}}>G/L Account Type :
          <select value={this.state.gType} onChange={this.handleChangeType}>
            {typeData.map((item) => (
              <option ref={this.glTypeEl} value={item.glType} required> {item.glType} {item.glTypeName} </option>
           ))}
          </select>

          </label>
          </div>



          <div className="select-container" >
          <label style={{paddingRight: '200px'}}>Department :
          <select value={this.state.depNo} onChange={this.handleChangeDep}>
            {depData.map((item) => (
              <option ref={this.departmentEl} value={item.department} required> {item.department} {item.description} </option>
           ))}
          </select>


          </label>
          </div>

           </fieldset>
     
           <p></p>
           
           <input type="submit" style={logstyle} className="Register" onClick={this.handleSubmit} name="submit" value="Update" />
           <button style={subStyle} name='back' onClick={this.handleBack} >Back</button>
         

           </form>

    


      )
    }
  };

export default glEdit;
