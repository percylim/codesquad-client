import React, { useState, useEffect, RadioButton, useRef } from 'react'
import Axios from 'axios';
//import { useHistory } from "react-router-dom";
import './Profile.css';
import ReactDOM from "react-dom";
import ExportPNLPDF from "./pdfPnLGenerator";
import { format } from "date-fns";
import moment from 'moment';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ReactTooltip from "react-tooltip";
//import {yearlyReport, quarterlyReport, monthlyReport} from "./salesReport";
import BootstrapTable from 'react-bootstrap-table-next';
import { AirlineSeatIndividualSuiteSharp, SettingsBackupRestoreRounded } from '@material-ui/icons';
import Moment from 'react-moment';
//import {CSVLink} from 'react-csv';
import PNLPDF from './pdfPnLGenerator';

import Blink from 'react-blink-text';

require('dotenv').config();//

const url = process.env.REACT_APP_SERVER_URL;
const companyID = localStorage.getItem('companyID');
const companyName = localStorage.getItem('companyName');
const userName = localStorage.getItem('userName');


// const userLevel = localStorage.getItem('userLevel');


var curr = new Date();
var todayDate = curr.toISOString().substr(0, 10);
var dDate= new Date(curr.setDate(curr.getDate()-7));
var finStartDate='';
var finEndDate='';
var finYear = dDate.getFullYear(); 
var data = [];
var gData=[];
var opData=[];
var totalArAmt =0 ;

 var sDate=''; //curr.toISOString().substr(0,10);
 var eDate= '';
 var date = new Date();
 var cMonth = date.getMonth()+12;
var stDate = new Date(date.getFullYear()+1, date.getMonth()-cMonth, 2);
var enDate = new Date(stDate.getFullYear(), stDate.getMonth()+12, 1);
var totalDebit=0;
var totalCredit =0;
var msg='';
var reportType='';
//var startDate=format(stDate, "dd/MM/yyyy");
//var endDate=format(enDate, "dd/MM/yyyy");

function BalanceSheet() {
  const [startDate, setStartDate] = useState(stDate.toISOString().substr(0,10));
  const [endDate, setEndDate] = useState(enDate.toISOString().substr(0,10)); 
  const [fixedAssetData, setFixedAssetData]= useState([]);
  const [currentAssetData, setCurrentAssetData]= useState([]);
  const [intangibleAssetData, setIntangibleAssetData]= useState([]);
  const [otherAssetData, setOtherAssetData]= useState([]);
  const [currentLiabilityData, setCurrentLiabilityData]= useState([]);
  const [longTermLiabilityData, setLongTermLiabilityData]= useState([]);
  const [equityData, setEquityData]= useState([]);
  const [totalFixedAsset, setTotalFixedAsset]=useState(0.00);
  const [totalCurrentAsset, setTotalCurrentAsset]=useState(0.00);
  
  //const [otherAssetData, setOtherAssetData] = useState([]);
 const negativeDisplay = async(amount) => {
  if (amount >= 0) { return amount;}
  return  '('+parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';  
 };
 const onSearchBalanceSheet = async() => {
  let FASData =[];
  let CASData = [];
  let prodData = [];
  let ARData =[];
  totalDebit=0;
  totalCredit=0;
  let sDate = new Date (startDate);
  let eDate = new Date(endDate);
  let firstDay = Number(String(sDate.getFullYear())+String(sDate.getMonth()+1).padStart(2, '0')+String(sDate.getDate()).padStart(2, '0'));
  let lastDay =  Number(String(eDate.getFullYear())+String(eDate.getMonth()+1).padStart(2,'0')+String(eDate.getDate()).padStart(2,'0'));
  
  let month=sDate.getMonth()+12;
  let totalFAS =0;
  //alert(monthEnd[month]+' - '+month);
   // if (lastDay-firstDay > monthEnd[month]) {
   //     alert('Ending Day cannot latter than the last day of the month');
   //     return false;
   // }
    
    if (isNaN(sDate.getTime())) {
      alert('Invalid Starting Date') ;
      return false;
    } 
    if (isNaN(eDate.getTime())) {
      alert('Invalid Ending Date') ;
      return false;
    } 
    if (eDate < sDate) {
      alert('Ending date must later than Starting date') ;
      return false;
    }
    let difference = eDate.getTime() - sDate.getTime();
    let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
    
    if (TotalDays > 365) {
      alert('Starting and Ending date cannot more than one year');
      return false;
    }
   // if (TotalDays < 365) {
   //   alert('Starting and Ending date must be one year');
  //    return false;
  //  }
   reportType='BAL'; 
   //alert(sDate+" = "+eDate);
   // get fixed Asset data
  Axios
  .get(url + `/fixedAsset`,
    {
      params: {
      companyID: companyID, 
      startDate: format(sDate,"yyyy-MM-dd"),
      endDate: format(eDate, "yyyy-MM-dd"),
      }
    }
  ).then(res => {
     
     if (res.data.length === 0) {
       alert('No G/L Account Selected');
       return false;
     } 
    // alert(res.data.length);
    for (let x = 0; x < res.data.length; x++) {   
      if (res.data[x].debit === null){
        res.data[x].debit=0;
      }    
      if (res.data[x].credit === null){
        res.data[x].credit=0;
      }  
     
        //  res.data[x].debit = dAmt ;//Math.abs(res.data[x].debit);
       //  }
         if (res.data[x].credit < 0) {
       //   res.data[x].credit = (res.data[x].credit * -1); //Math.abs(res.data[x].credit);
         } 
         if (res.data[x].debit < 0) {
          res.data[x].debit = (res.data[x].debit * -1); //Math.abs(res.data[x].credit);
          
          } 
     //     alert(res.data[x].debit+' - '+res.data[x].credit);
      // totalDebit+=res.data[x].debit;
      // totalCredit+=res.data[x].credit; 
        let addNo='';
        let amt =0;
        if (res.data[x].debit > 0) {
            amt=res.data[x].debit;   
        } else {
            amt = res.data[x].credit;
        }
         if (res.data[x].glType === '406') {
            addNo = 'Less :';
          //  totalDebit-= amt; //res.data[x].debit;
            totalCredit-= amt; // res.data[x].credit; 
         } else {
         // totalDebit+=res.data[x].debit;
          totalCredit+= amt; //res.data[x].credit; 
         }
         //  totalFAS+=amt;
    //   alert(totalCredit); 
        // setTotalRev(totalCredit);
        if (res.data[x].debit !==0 || res.data[x].credit !==  0) {  
       let newData = {
        addNo: addNo,
        glName: res.data[x].glName,
        totalText: '', // res.data[x].glType+' - '+res.data[x].glNo+' - '+res.data[x].glSub,
        amount: amt, // res.data[x].debit-res.data[x].credit,
        }
        FASData.push(newData); 
     } // if 
  
    } //for
      let FASAmt=0  
      setFixedAssetData(FASData);
     for (let y = 0; y < FASData.length; y++) {   
       // if (fixedAssetData[y].addNo === 'Less :') {
       //     FASAmt-=fixedAssetData[y].amount;
       // } else {
             FASAmt+=FASData[y].amount;
       // }
     } 
      
      setTotalFixedAsset(FASAmt);
    //  alert(FASAmt);
    })

     //  load current assets information;
     Axios
     .get(url + `/currentAsset`,
       {
         params: {
         companyID: companyID, 
         startDate: format(sDate,"yyyy-MM-dd"),
         endDate: format(eDate, "yyyy-MM-dd"),
         }
       }
     ).then(res => {
        
        if (res.data.length === 0) {
          alert('No G/L Account Selected');
          return false;
        } 
       // alert(res.data.length);
       for (let x = 0; x < res.data.length; x++) {   
         if (res.data[x].debit === null){
           res.data[x].debit=0;
         }    
         if (res.data[x].credit === null){
           res.data[x].credit=0;
         }  
        
           //  res.data[x].debit = dAmt ;//Math.abs(res.data[x].debit);
          //  }
            if (res.data[x].credit < 0) {
          //   res.data[x].credit = (res.data[x].credit * -1); //Math.abs(res.data[x].credit);
            } 
            if (res.data[x].debit < 0) {
             res.data[x].debit = (res.data[x].debit * -1); //Math.abs(res.data[x].credit);
             
             } 
        //     alert(res.data[x].debit+' - '+res.data[x].credit);
         // totalDebit+=res.data[x].debit;
         // totalCredit+=res.data[x].credit; 
           let addNo='';
           let amt =0;
           if (res.data[x].debit > 0) {
               amt=res.data[x].debit;   
           } else {
               amt = res.data[x].credit;
           }
          
            //  totalFAS+=amt;
       //   alert(totalCredit); 
           // setTotalRev(totalCredit);
           if (amt < 0 ) {addNo =' Less :'}
           if (res.data[x].debit !==0 || res.data[x].credit !==  0) {  
          let newData = {
           addNo: addNo,
           glName: res.data[x].glName,
           totalText: '', // res.data[x].glType+' - '+res.data[x].glNo+' - '+res.data[x].glSub,
           amount: amt, // res.data[x].debit-res.data[x].credit,
           }
           CASData.push(newData); 
        } // if 
     
       } //for
         let CASAmt=0  
       //  setCurrentAssetData(CASData);
        for (let y = 0; y < CASData.length; y++) {   
          // if (fixedAssetData[y].addNo === 'Less :') {
          //     FASAmt-=fixedAssetData[y].amount;
          // } else {
                CASAmt+=CASData[y].amount;
          // }
        }
         setTotalCurrentAsset(CASAmt);
       //  alert(FASAmt);
       })
      // load account receivable
 
        
          let CASAmt=0  
          setCurrentAssetData(CASData);
         for (let y = 0; y < CASData.length; y++) {   
           // if (fixedAssetData[y].addNo === 'Less :') {
           //     FASAmt-=fixedAssetData[y].amount;
           // } else {
                 CASAmt+=CASData[y].amount;
           // }
         } 
      //   alert(CASAmt);
          setTotalCurrentAsset(CASAmt);
       //   alert(CASAmt);
    //    })
     // alert('here');
     totalArAmt=0;
      Axios
      .get(url + `/accountReceivable`,
        {
          params: {
          companyID: companyID, 
          startDate: format(sDate,"yyyy-MM-dd"),
          endDate: format(eDate, "yyyy-MM-dd"),
          }
        }
      ).then(res => {
         let arAmt =0;
      //   if (res.data.length === 0) {
      //     alert('No Account Receivable Selected');
      //     return false;
      //   } 
     //    alert(res.data.length);
        for (let x = 0; x < res.data.length; x++) {   
          if (res.data[x].debit === null){
            res.data[x].debit=0;
          }    
          if (res.data[x].credit === null){
            res.data[x].credit=0;
          }  
         
            //  res.data[x].debit = dAmt ;//Math.abs(res.data[x].debit);
           //  }
             if (res.data[x].credit < 0) {
              res.data[x].credit = (res.data[x].credit * -1); //Math.abs(res.data[x].credit);
             } 
             if (res.data[x].debit < 0) {
              res.data[x].debit = (res.data[x].debit * -1); //Math.abs(res.data[x].credit);
              
              } 
         //     alert(res.data[x].debit+' - '+res.data[x].credit);
          // totalDebit+=res.data[x].debit;
          // totalCredit+=res.data[x].credit; 
          
                arAmt+=res.data[x].debit-res.data[x].credit;
                
         } //for   
              
             //  totalFAS+=amt;
        //   alert(totalCredit); 
            // setTotalRev(totalCredit);
      //      if (res.data[x].debit !==0 || res.data[x].credit !==  0) {  
           let newData = {
            addNo: '',
            glName: 'Account Receivable',
            totalText: '', // res.data[x].glType+' - '+res.data[x].glNo+' - '+res.data[x].glSub,
            amount: arAmt, // res.data[x].debit-res.data[x].credit,
            }
            ARData.push(newData); 
            setOtherAssetData(ARData);
            totalArAmt=arAmt;
         //   alert(totalArAmt);
           // CASAmt=CASAmt+arAmt;
          // setTotalCurrentAsset(arAmt);
          //  setOtherAssetData(ARData);
        // } // if 
          })     

 }

 const formatInputStartDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
   //  alert(e.target.value);
    setStartDate(dDate);
  //  setData([]);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
  //  inputRef.current.focus();
  };
  const formatInputeDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setEndDate(dDate);
  //  setData([]);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
  //  inputRef.current.focus();
  };
  const formatInputsDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setStartDate(dDate);
    //   alert(dDate);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
   // inputRef.current.focus();
  };
  const formatInputEndDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setEndDate(dDate);
  
  };
  const onSave = async () => {


  };

   const onClear = async () => {
  /*  
    totalDebit=0;
    totalCredit=0;
     setRevData([]);
     setCostData([]);
     setExpData([]);
     msg='';
     setTotalRev(0.00);
     setTotalCostOfSales(0.00);
     setTotalExpenses(0.00);
     profit=0;
     setTaxOnYear(0.00);
  */   
  };  
  const onHome = async () => {
        window.location='home';
   
   };
  const onPrint = async () => {
   //  alert('totalRev: '+totalRev+' - totalCostOfSales: '+totalCostOfSales+' - totalExpenses: '+totalExpenses+' - profit: '+profit+' - taxOnYear: '+taxOnYear);
  //  if(revData.length === 0) {
  //    alert('P&L Revenue section is blank for printing');
  //    return false;
  //   }
   //  if(costData.length === 0) {
   //   alert('P&L Cosy Of Sales section is blank for printing');
   //   return false;
   //  }  
   //  if(expData.length === 0) {
   //   alert('P&L Expenditure section is blank for printing');
   //   return false;
   //  } 
  };

 return (


    
        
      

   
<div>
      <div className="row">
      
        <div className="col-sm-12" style={{ marginTop: '1px', backgroundColor: '#c1f8ae', color: 'black' }}>
          <h2>Balance Sheet</h2>
        </div>
      </div>
 
      <div style={{
  display: 'inline-block',
  width: '1520px',
  height: '50px',
  margin: '6px',
  backgroundColor: 'white',
  border: '4px solid grey',
}}>
<label style={{ paddingLeft: "100px", marginTop: '.4rem'}}>
<a style={{  marginRight: '.8rem' }} >Balance Sheet From Date : </a>
    <input
      type="date"
      maxLength={10}
      value={startDate}
      style={{ width: '10%', border: '1px solid #696969' }}
    //  defaultValue = {sDate}
      onChange={(e) => formatInputsDate(e)}   
      onBlur={(e) => formatInputStartDate(e) }
      name="startDate"
      required
      disabled={false}
      data-tip data-for="sDateTip"
    />
<ReactTooltip id="sDateTip" place="top" effect="solid">
    Select Balance Sheet Starting Date 
</ReactTooltip>

    <a style={{ marginLeft: '1rem', marginRight: '.8rem' }} >To Date : </a>
            <input
              type="date"
              maxLength={10}
              value={endDate}
        //      defaultValue={eDate}
              style={{ marginRight: '2rem', width: '10%', border: '1px solid #696969' }}
              name="endDate"
             onChange={(e) => formatInputeDate(e)}
              onBlur={(e) => formatInputEndDate(e) }
              required
              disabled={false}
              data-tip data-for="eDateTip"
            />

     
<ReactTooltip id="eDateTip" place="top" effect="solid">
    Select Balance SHeet Ending Date 
</ReactTooltip>  

<button
        style={{ padding: '4px', marginLeft: '2rem' }}
        type='button'
        class='btn btn-info fa fa-download float-right'
        onClick={() => onSearchBalanceSheet()}
        data-tip data-for="monthTip"
      >Load Balance Sheet Report</button>
<ReactTooltip id="monthTip" place="top" effect="solid">
    Press to generate Balance Sheet Report
</ReactTooltip>

<button
        style={{ padding: '1px', marginLeft: '2rem' }}
        type='button'
        class='btn btn-danger float-right'
        onClick={() => onClear()}
        data-tip data-for="clearTip"
      >Clear</button>
<ReactTooltip id="clearTip" place="top" effect="solid">
    Press to Clear Report Listing
</ReactTooltip>
<button
        style={{ padding: '4px', marginLeft: '2rem' }}
        type='button'
        class='btn btn-warning fa fa-print float-right'
        onClick={() => onPrint()}
        data-tip data-for="printTip"
      >Print</button>
<ReactTooltip id="printTip" place="top" effect="solid">
    Press to Print Balance Sheet
</ReactTooltip>
<button
        style={{ padding: '1px', marginLeft: '2rem' }}
        type='button'
        class='btn btn-success'
        onClick={() => onHome()}
        data-tip data-for="homeTip"
      >Home</button>
<ReactTooltip id="homeTip" place="top" effect="solid">
    Press to return Home
</ReactTooltip>
<ReactTooltip id="taxOnYearTip" place="top" effect="solid">
    Press to Generate Tax on The Year 
</ReactTooltip>
<button

            style={{ backgroundColor: '#5b5959', color: 'white', height: '28px',width: '200px', marginLeft: '1rem', paddingTop: '1px'}}
            type='button'
            class='btn btn-Default'
            onClick={() => onSave()}
            data-tip data-for="saveTip"
          >Save Yearly Report</button>
<ReactTooltip id="saveTip" place="top" effect="solid">
        Press to Save to yearly Report for future retrieve
</ReactTooltip>

</label>

</div> 

<div className="row" style={{margin: '20px'}}>
      
      <div className="col-sm-12" style={{ marginTop: '1px', color: 'black' }}>
      
     
      </div>
    </div> 
       
      <span class="square border border-dark"></span>
      <table class="table" style={{ paddingTop: '1px', paddingLeft: '50px', border: '1px solid black' }}>
             <thead class="thead-dark" >
               <tr style={{ align: 'left' }}>          
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '70px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '1000px', textAlign: 'center' }}>FIXED ASSETS</th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}>RM</th>
             
                  </tr>
             </thead>
             <tbody style={{align:'left'}} >
               {fixedAssetData.map(item => {
                 return <tr key={item.id}>
   
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.addNo}</td>
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.glName}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right', backgroundColor: '#f5f0f0' }}>{item.totalText}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right' }}>{parseFloat(item.amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
              
           
                 </tr>
   
               })}
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               </tbody>
             <tfoot>
              
   
               <td></td><td></td> 
               <td class="square border border-dark" style={{ textAlign: "center",  backgroundColor: "#eae4e4" }}>TOTAL FIXED ASSETS :</td>           
               <td class="square border border-dark" style={{ textAlign: "right",backgroundColor: "#eae4e4" , color: "red" }}>{parseFloat(totalFixedAsset).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
        
               <td></td>
               <td></td>
               <td></td>
             </tfoot>
           </table>

 
   

<table class="table" style={{ paddingTop: '1px', paddingLeft: '50px', border: '1px solid black' }}>
             <thead class="thead-dark" >
               <tr style={{ align: 'left' }}>          
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '70px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '1000px', textAlign: 'center' }}>CUREENT ASSET</th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}>RM</th>
             
                  </tr>
             </thead>
             <tbody style={{align:'left'}} >
               {currentAssetData.map(item => {
                 return <tr key={item.id}>
   
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.addNo}</td>
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.glName}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right', backgroundColor: '#f5f0f0' }}>{item.totalText}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right' }}>{parseFloat(item.amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
              
           
                 </tr>
   
               })}





               <td></td>
               <td></td>
               <td></td>
               <td></td>
               </tbody>
           
      
               <thead class="thead-dark" >
               <tr style={{ align: 'left' }}>          
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '70px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '1000px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}></th>
                 <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}></th>
             
                  </tr>
             </thead>
             <tbody style={{align:'left'}} >
               {otherAssetData.map(item => {
                 return <tr key={item.id}>
   
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.addNo}</td>
                   <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.glName}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right', backgroundColor: '#f5f0f0' }}>{item.totalText}</td>
                   <td class="square border border-dark" style={{ textAlign: 'right' }}>{parseFloat(item.amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
              
           
                 </tr>
   
               })}





               <td></td>
               <td></td>
               <td></td>
               <td></td>
               </tbody>










             <tfoot>
              
   
               <td></td><td></td> 
               <td class="square border border-dark" style={{ textAlign: "center",  backgroundColor: "#eae4e4" }}>TOTAL CURRENT ASSETS :</td>           
               <td class="square border border-dark" style={{ textAlign: "right",backgroundColor: "#eae4e4" , color: "red" }}>{parseFloat(totalCurrentAsset+totalArAmt).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
        
               <td></td>
               <td></td>
               <td></td>
             </tfoot>
           </table>





</div>

); // return
}; // function
export default BalanceSheet;
   