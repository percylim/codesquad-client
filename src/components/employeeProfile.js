import React, { useState, useEffect, useRef } from 'react'
import Axios from 'axios';
import { useHistory } from "react-router-dom";
import EscapeStr from './mysqlConvertChar';
import ReactTooltip from "react-tooltip";
const fetch = require('node-fetch');
const companyID = localStorage.getItem('companyID');
require('dotenv').config();//
 const url = process.env.REACT_APP_SERVER_URL;
 var empNo = sessionStorage.getItem('employeeNo');
 var companyName = localStorage.getItem('companyName')

function EmployeeProfile() {
const inputReference = useRef(null);   
const [employeeNo, setEmployeeNo] = useState('');
const [employeeName, setEmployeeName] = useState('');
    
const formatInputUserID = (e) => {
   setEmployeeNo(e.target.value);
};
const formatInputUserName = (e) => {
    setEmployeeName(e.target.value);
 } ;    

useEffect(() => {
    inputReference.current.focus();
});    

return (
    <div>
    
    <div className="App">
    
      <div className="col-sm-12" style={{ marginTop: '1px',  color: 'black' }}>
    
        <h2>User Profile And Password Maintenance</h2>
      </div>
      </div>
      <div style={{
      display: 'inline-block',
      width: '1550px',
      height: '170px',
      margin: '6px',
      backgroundColor: 'white',
      border: '4px solid grey',
    }}>

    
   <p></p>
<label style={{ paddingLeft: "10px"}} >
          <a style={{ marginRight: '2rem' }}> User ID : </a>
          <input
            type="text"
            style={{ width: '70px', border: '1px solid #696969', marginRight: '2rem' }}
            value={employeeNo}
            maxLength={30}
            manLength={4}
            name="employeeNo"
            ref={inputReference}
            class="text-uppercase"
            onChange={(e) => formatInputUserID(e)}
            required
            readOnly = {false}
            data-tip data-for="empNoTip"
          />
       <ReactTooltip id="empNoTip" place="top" effect="solid">
        Enter a User ID with 4 - 30 characters 
      </ReactTooltip>
      <a>User Name :</a> 
      <input
            type="text"
            style={{ width: '500px', border: '1px solid #696969', marginLeft: '.5rem' }}
            value={employeeName}
            maxLength={200}
            name="employeeName"
            onChange={(e) => formatInputUserName(e)}
            required
            readOnly = {false}
            data-tip data-for="empNameTip"
          />
       <ReactTooltip id="empNameTip" place="top" effect="solid">
        Enter a User Name with maximum 200 characters 
      </ReactTooltip>

  </label>

    </div>
    </div>

); // return
}; // function

export default EmployeeProfile

