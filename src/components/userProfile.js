import  React from 'react';
// mport validator from 'validator'
// import ReactDOM from 'react-dom';
import EscapeStr from './mysqlConvertChar';
//import moment from 'moment';
import Axios from "axios";

import  './UserProfile.css';
require('dotenv').config();//
const url = process.env.REACT_APP_SERVER_URL;

const fetch = require('node-fetch');
var lastSix = '';
var sex = 'M';
var paytype='M';
var married = 'M';
var Level = 4;
var lEdit = false;
const companyID = localStorage.getItem('companyID');
var userLevel = localStorage.getItem('userLevel');
const companyName = localStorage.getItem('companyName')
// var eDate = '';
//  var sDate = '';
class UserProfile extends React.Component {
    constructor(props) {
      super(props);
      // create a ref to store the DOM element
     // this.state = { usersCollection: [] };
      this.state = {
        input: {},
        errors: {},
        data: [],
        number: 0,
        state: {},
        name: [],
        Level: [],
        gender: 'M',
        payment: 'M',
        marital: 'S',
        level: 4,


      };


      this.handleInputChange = this.handleInputChange.bind(this);
      this.formatInput = this.formatInput.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleChangeType = this.handleChangeType.bind(this);
      this.handleChangeMarried = this.handleChangeMarried.bind(this);
      this.handleChangeLevel = this.handleChangeLevel.bind(this);
    //   this.onSearchUser = this.onSearchUser.bind(this);

      this.companyIDEl = React.createRef();
      this.employeeNoEl = React.createRef();
      this.employeeNameEl = React.createRef();
      this.nricEl = React.createRef();
      this.sexEl = React.createRef();
  
      this.raceEl = React.createRef();

      this.address1El = React.createRef();
      this.address2El = React.createRef();
      this.cityEl = React.createRef();
      this.stateEl = React.createRef();
      this.postcodeEl = React.createRef();
      this.countryEl = React.createRef();
      this.levelEl = React.createRef();
     
      this.telNoEl = React.createRef();
      this.hpNoEl = React.createRef();
      this.emailEl = React.createRef();
     
      this.passwordEl = React.createRef();
      this.handleSubmit = this.handleSubmit.bind(this);
    }


   

    handleChange(event) {

      this.setState({gender: event.target.value});
      sex = event.target.value
     // this.sexEl.current.value=value;
    //  alert(sex);
   }
   handleChangeType(event) {

    this.setState({payment: event.target.value});
    paytype = event.target.value
   // this.sexEl.current.value=value;
  //  alert(sex);
 }
 handleChangeMarried(event) {

  this.setState({marital: event.target.value});
  married = event.target.value;
 // this.sexEl.current.value=value;
//  alert(sex);
}

 handleChangeLevel(e) {

      this.setState({ level: e.target.value });
      Level = e.target.value;
   //   alert(Level);
    }

    handleSubmit(e) {
     // alert("#0");
      e.preventDefault();

      if(this.validate()){
        console.log(this.state);
       // alert(this.sexEl.current.value);

        const user= {
          companyID: EscapeStr(companyID),
          companyName: EscapeStr(companyName),
          employeeNo:EscapeStr(this.employeeNoEl.current.value.toUpperCase()),
          employeeName: EscapeStr(this.employeeNameEl.current.value),
          nric: this.nricEl.current.value,
          sex: sex,
          dateBirth: '0000-00-00',
          race: EscapeStr(this.raceEl.current.value),
          maritalStatus: '',
          childs: 0,
          address1: EscapeStr(this.address1El.current.value),
          address2: EscapeStr(this.address2El.current.value),
          city: EscapeStr(this.cityEl.current.value),
          state: EscapeStr(this.stateEl.current.value),
          postcode: this.postcodeEl.current.value,
          country: EscapeStr(this.countryEl.current.value),
          level: Level,
          position: '',
          salary: 0,
          payMethod: '',
          incomeTaxNo: '',
          epfNo: '',
          socsoNo: '',
          drivingLicenseNo: '',
          telNo: EscapeStr(this.telNoEl.current.value),
          hpNo: EscapeStr(this.hpNoEl.current.value),
          email: EscapeStr(this.emailEl.current.value),
          employDate: '',
          password: this.passwordEl.current.value,
         };
         //var name1 =  EscapeStr(user.companyName);
       // alert(Level);
      // return false
        fetch(url+'/employeeNew', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify( user )
          // We convert the React state to JSON and send it as the POST body
         // data: JSON.stringify(user,user.ame)
          }).then(function(response) {
           return response.text()
        }).then(function(text) {


          // alert(text);
         lastSix = text.substr(text.length - 7); // => "Tabs1"
          //  poemDisplay.textContent = text;
          // alert(lastSix);

           if (lastSix === 'Success') {
            localStorage.clear();
            localStorage.setItem('companyName', user.companyName);
            window.location.reload(false);
           };
          });
         // reset to null value

       // this.companyNameEl.current.value = "";
       this.employeeNoEl.current.value = "";
       this.employeeNameEl.current.value = "";
       this.nricEl.current.value = "";
       this.sexEl.current.value = "";
       this.raceEl.current.value = "";
       this.address1El.current.value = "";
       this.address2El.current.value = "";
       this.cityEl.current.value = "";
       this.stateEl.current.value = "";
       this.postcodeEl.current.value = "";
       this.countryEl.current.value = "";
       this.levelEl.current.value = 5;
       this.telNoEl.current.value = "";
       this.hpNoEl.current.value = "";
       this.emailEl.current.value = "";
       this.passwordEl.current.value = "";
    }



  }



    validate(){

       // let input = this.state.input;
       // let errors = {};
      //  let isValid = true;
    //   if (this.companyIDEl.current.value.length < 8){
    //   alert("Company ID must be from 8 - 20 character");
     //  return false;
     //  }
     let  prod=this.employeeNoEl.current.value;
     for (let i = 0; i < prod.length; i++) {
         if (prod.substr(i,1) === ';') {
           alert("Employee No. cannot contain (;) letter ");
           return false;
         }

     }

      return true;
    }

 allowOnlyNumericsOrDigits(e) {
		const charCode = e.which ? e.which : e.keyCode;

		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			this.setState({ error: 'OOPs! Only numeric values or digits allowed' });
		}
	};

//  handleChange(e) {
 //   this.setState({ value: e.target.value});
 // }

//handleBlur(e) {
//  var num = parseFloat(this.state.value);
//  var cleanNum = num.toFixed(2);
//  this.setState({value: cleanNum});
//}


onValueChange = (event) => {
  this.setState({value:event.target.value})
}



  handleInputChange(event) {
    console.log(event.target.value)
    this.setState({
      number: event.target.value
    })
  }

  formatInput() {
    const num = this.state.number
    this.setState({
        number: parseFloat(num).toFixed(2)
    })
  }

   






    render() {
        const mystyle = {
            color: "BLACK",
            backgroundColor: "#ffffff",
            padding: "5px 15px 10px 10px",
            alignItems: "left",
            fontFamily: "Arial",



        };
  //  const toInputLowercase = e => {
  //  e.target.value = ("" + e.target.value).toLowerCase();
  //  };

         const buttonStyle = {
          color: "black",
          backgroundColor: "yellow",
          padding: "10px 15px 10px 10px",
          fontFamily: "Arial",
          position: 'absolute',
          right: 350,
      };

   const subStyle = {
          color: "white",
          backgroundColor: "blue",
          padding: "10px 15px 10px 10px",
          fontFamily: "Arial",

      };
      const logstyle = {
          color: "white",
          backgroundColor: "red",
          padding: "5px 15px 10px 10px",
          fontFamily: "Arial",
          position: 'absolute',
          right: 740,
          width: '7em',
          height: '2.8rem',

      };

      const onSave = () => {
         if (this.passwordEl.current.value === '') {
          alert('Password cannot be blank');
          return false
         }
        const user= {
          companyID: EscapeStr(companyID),
          companyName: EscapeStr(companyName),
          employeeNo:EscapeStr(this.employeeNoEl.current.value.toUpperCase()),
          employeeName: EscapeStr(this.employeeNameEl.current.value),
          nric: this.nricEl.current.value,
          sex: sex,
          dateBirth: '0000-00-00', 
          race: EscapeStr(this.raceEl.current.value),
          maritalStatus: '',
          childs: 0,
          address1: EscapeStr(this.address1El.current.value),
          address2: EscapeStr(this.address2El.current.value),
          city: EscapeStr(this.cityEl.current.value),
          state: EscapeStr(this.stateEl.current.value),
          postcode: this.postcodeEl.current.value,
          country: EscapeStr(this.countryEl.current.value),
          level: Level,
          position: '',
          salary: 0,
          payMethod: '',
          incomeTaxNo: '',
          epfNo: '',
          socsoNo: '',
          drivingLicenseNo: '',
          telNo: EscapeStr(this.telNoEl.current.value),
          hpNo: EscapeStr(this.hpNoEl.current.value),
          email: EscapeStr(this.emailEl.current.value),
          employDate: '',
          password: this.passwordEl.current.value,
         };  
        if (lEdit === true) {     
        fetch(url+'/employeeUpdate', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify( user )
          // We convert the React state to JSON and send it as the POST body
         // data: JSON.stringify(user,user.ame)
          }).then(function(response) {
           return response.text()
        }).then(function(text) {
    
            if (text === 'success'){
              alert('User updated');  
              return true
            } else {
              alert('User update failed');
              return false
            }
        
        }
        )

        } else {

          fetch(url+'/employeeNew', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify( user )
            // We convert the React state to JSON and send it as the POST body
           // data: JSON.stringify(user,user.ame)
            }).then(function(response) {
             return response.text()
          }).then(function(text) {
             if (text === 'success') {
               alert('New User added');
               return true;
             } else {
               alert('New User add failed');
               return false;
             }
          })  
            lEdit = false; 

        }
      };
      const onSearchUser= () => {
         if (this.employeeNoEl.current.value === '') {
          lEdit = false;
          return true
         }

        Axios
        .get(url + `/userSearch`,
          {
            params: {
              companyID: companyID,
               employeeNo: this.employeeNoEl.current.value,
            }
          }
        ).then(res => {
            if (res.data.length > 0 ) {  
               if (res.data[0].level<userLevel) {
                alert('Sorry you are not authorised to modify the Selected User');
                return false;
               }       
            this.employeeNameEl.current.value = res.data[0].employeeName; 
            this.nricEl.current.value = res.data[0].nric;
            this.sexEl.current.value = res.data[0].sex;
          
            this.raceEl.current.value = res.data[0].race;
            this.address1El.current.value = res.data[0].address1;
            this.address2El.current.value = res.data[0].address2;
            this.cityEl.current.value = res.data[0].city;
            this.stateEl.current.value = res.data[0].state;
            this.postcodeEl.current.value= res.data[0].postCode;
            this.countryEl.current.value = res.data[0].country; 
            this.levelEl.current.value = res.data[0].level;  
            this.telNoEl.current.value = res.data[0].telNo;
            this.hpNoEl.current.value = res.data[0].hpNo;
            this.emailEl.current.value = res.data[0].email;
            this.setState({ level: res.data[0].level });
            this.setState({ gender: res.data[0].sex });
            let cur = new Date(res.data[0].dateBirth);
         //   cur.setDate(cur.getDate());
         //   let sDate = cur.toISOString().substr(0,10);
           // alert(sDate);
         //   this.dateBirthEl.current.value = sDate;
            lEdit = true;
            } else {
              alert("New User");
              lEdit = false;  
            }
         
        });
  
    



      }


      const onDelete= () => {
        if (this.employeeNoEl.current.value === '') {
          alert('No User Loaded');
          return true;
        } 
         if (lEdit === false) {
          alert('This is new user not update yet');
          return true;
         }
      //  alert(userLevel+ ' = '+this.levelEl.current.value); 
        if (userLevel>this.levelEl.current.value) {
          alert('Sorry your are not authorise to delete this User ')
          return false;
        }  
        if (window.confirm('Confirm to delete this user>')) {
      //   // delete user
      let user = {  companyID: companyID, employeeNo: this.employeeNoEl.current.value}
      fetch(url+'/employeeDelete', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify( user )
        // We convert the React state to JSON and send it as the POST body
       // data: JSON.stringify(user,user.ame)
        }).then(function(response) {
         return response.text()
      }).then(function(text) {

        if (text === 'success') {
          alert('user has been deleted');

        } 
          
 
      })    
         }

         return true;

      };

      const onCancel= () => {


        this.employeeNoEl.current.value = "";
        this.employeeNameEl.current.value = "";
        this.nricEl.current.value = "";
        this.sexEl.current.value = "";
        this.raceEl.current.value = "";
        this.address1El.current.value = "";
        this.address2El.current.value = "";
        this.cityEl.current.value = "";
        this.stateEl.current.value = "";
        this.postcodeEl.current.value = "";
        this.countryEl.current.value = "";
        this.levelEl.current.value = 5;
        this.telNoEl.current.value = "";
        this.hpNoEl.current.value = "";
        this.emailEl.current.value = "";
        this.passwordEl.current.value = "";
        lEdit = false;

     };


     const list = [
      {label: '5 - general staff',
      value: 5,
      },
       {label: '4 - general clerk',
       value: 4,
       },
        {label: '3 - account clerk',
         value: 3,
        },
        {label: '2 - accountant',
         value: 2,
        },
        {label: '1 - management officer',
          value: 1,
        }
      ];

     const options= list.slice(0,5-userLevel);

     // Defining our N

      // alert(options[1].label);
     // const [errorMessage, setErrorMessage] = useState('');
     // const validateDate = (value) => {
     //   alert("here");
     //   if (validator.isDate(value)) {
     //     setErrorMessage('Valid Date :)')
     //   } else {
     //     setErrorMessage('Enter Valid Date!')
     //   }
     // }



      return (


        <form style={mystyle}>


          <fieldset>

           <h1>Add / Edit / Delete User Profile</h1>
           <label style={{paddingRight: '240px'}} >User ID :
          <input style={{marginLeft: '1.8rem', border: '1px solid #696969' }} class="text-uppercase" minlength={4} maxLength={20} ref={this.employeeNoEl} name="employeeNo" required />
         
          <button
              style={{ padding: '10px' }}
              type='button'
              class='btn btn-primary fa fa-download float-right'
              onClick={onSearchUser}
            ></button>
         
          </label>
           <label style={{paddingRight: '250px'}}>User Name :
          <input style={{marginLeft: '0.3rem', border: '1px solid #696969' }} type="text" maxLength={200} ref={this.employeeNameEl} name="employeeName" required />
          </label>

          <label  style={{paddingRight: '200px'}}>NRIC No. :
          <input style={{marginLeft: '1rem', border: '1px solid #696969'  }} class="text" type="text" maxLength={20} ref={this.nricEl} name="nric" required/>
          </label>


           <label style={{paddingRight: '620px'}} component="legend">Gender :
              <td style={{paddingLeft: '103px'}}>
              <input type="radio"  value="M" name="gender"  onChange={this.handleChange} ref={this.sexEl} checked={this.state.gender === 'M'} /> Male
              </td>

               <td style={{color: 'red'}}>
               <input type="radio" value="F" name="gender" onChange={this.handleChange} ref={this.sexEl} checked={this.state.gender === 'F'} /> Female
               </td>

               <td>
               <input type="radio" value="O" name="gender"  onChange={this.handleChange} ref={this.sexEl}  checked={this.state.gender === 'O'}/> Other

               </td>
             
             </label>



        

          <label style={{paddingRight: '80px'}}>Race :


          <input  style={{marginLeft: '3.2rem',  border: '1px solid #696969' }} type="text" ref={this.raceEl} name="race"  required/>

          </label>

         

         
          <label style={{paddingRight: '180px'}}>Address #1 :
          <input style={{marginLeft: '.6rem', border: '1px solid #696969'  }} type="text" maxLength={200} ref={this.address1El} name="address1" required/>
          </label>
          <label style={{paddingRight: '180px'}}>Address #2 :
          <input style={{marginLeft: '.6rem', border: '1px solid #696969' }} type="text" maxLength={200} ref={this.address2El} name="address2" />
          </label>
          <label style={{paddingRight: '50px'}}>City :
          <input style={{marginLeft: '4rem', border: '1px solid #696969' }} type="text" maxLength={50} ref={this.cityEl} name="city" required />
          </label>
          <label style={{paddingRight: '65px'}}>State :
          <input style={{marginLeft: '3.5rem', border: '1px solid #696969' }} type="text" maxLength={50} ref={this.stateEl} name="state" required />
          </label>
          <label style={{paddingRight: '180px'}}>Post Code :
          <input style={{marginLeft: '1.2rem', border: '1px solid #696969' }} type="text" maxLength={50} ref={this.postcodeEl} name="postcode" required />
          </label>
          <label style={{paddingRight: '120px'}}>Country :
          <input style={{marginLeft: '2.4rem', border: '1px solid #696969' }} type="text" maxLength={50} ref={this.countryEl} name="country" required />
          </label>
          <label style={{paddingLeft: '230px'}}>Level :

                  
                    <select style={{marginLeft: '3.6rem'}} value={this.state.level} onChange={this.handleChangeLevel}>
                      {options.map((option) => (
                        <option ref={this.levelEl} value={option.value} required>{option.label} </option>
                     ))}
                    </select>
                          

          </label>

      
         
          <label style={{paddingRight: '240px'}}>Telephone :
          <input style={{marginLeft: '1.5rem', border: '1px solid #696969' }} type="text" maxLength={20} ref={this.telNoEl} name="telNo" />
          </label>
          <label style={{paddingRight: '260px'}}>Hand phone :
          <input style={{marginLeft: '.6rem', border: '1px solid #696969' }} type="text" maxLength={20} ref={this.hpNoEl} name="hplNo" />
          </label>
          <label style={{paddingRight: '80px'}}>Email :
          <input style={{marginLeft: '3.5rem', border: '1px solid #696969' }} type="email"  maxLength={100} ref={this.emailEl} name="email"/>
          </label>
        
           <label style={{paddingRight: '180px'}}> Password:
           <input style={{marginLeft: '2rem', border: '1px solid #696969' }} type="password" ref={this.passwordEl} maxLength={15} />
           </label>
            <p></p>

           </fieldset>
           <p></p>
           <p>
           <button style={logstyle} onClick={onSave}> Save User  </button>
           <button style={buttonStyle} onClick={onCancel}>New User</button>
           <button style={subStyle} onClick={onDelete}>Delete User</button>
           </p>



        </form>
      )
    }
  };

export default UserProfile;
