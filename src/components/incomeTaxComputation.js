import React, { useState, useEffect, RadioButton, useRef } from 'react'
import Axios from 'axios';
//import { useHistory } from "react-router-dom";
import './Profile.css';
import ReactDOM from "react-dom";
import generatePDF from "./reportGenerator";
import { format } from "date-fns";
import moment from 'moment';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ReactTooltip from "react-tooltip";
//import {yearlyReport, quarterlyReport, monthlyReport} from "./salesReport";
import BootstrapTable from 'react-bootstrap-table-next';
import { AirlineSeatIndividualSuiteSharp, SettingsBackupRestoreRounded } from '@material-ui/icons';
import Moment from 'react-moment';
//import {CSVLink} from 'react-csv';
import ExportTrialBalancePdf from './pdfTrialBalanceGenerator';
import Blink from 'react-blink-text';
require('dotenv').config();//
const url = process.env.REACT_APP_SERVER_URL;
const companyID = localStorage.getItem('companyID');
const companyName = localStorage.getItem('companyName');
const userName = localStorage.getItem('userName');


// const userLevel = localStorage.getItem('userLevel');


var curr = new Date();
var cYear = 'Years';
var todayDate = curr.toISOString().substr(0, 10);
var dDate= new Date(curr.setDate(curr.getDate()-7));
var finStartDate='';
var finEndDate='';
var finYear = dDate.getFullYear(); 
 var date = new Date();
 var cMonth = date.getMonth()+12;
var stDate = new Date(date.getFullYear()+1, date.getMonth()-cMonth, 2);
var enDate = new Date(stDate.getFullYear(), stDate.getMonth()+12, 1);


var yearSelected=finYear;
//var startDate=format(stDate, "dd/MM/yyyy");
//var endDate=format(enDate, "dd/MM/yyyy");
var yearOption = [{label: finYear-2, value: finYear-2}, {label: finYear-1, value: finYear-1}, {label: finYear, value: finYear}, finYear];
function IncomeTaxComputation() {
  const [startDate, setStartDate] = useState(stDate.toISOString().substr(0,10));
  const [endDate, setEndDate] = useState(enDate.toISOString().substr(0,10))
   const [revData, setRevDate] = useState([]);
   const [totalRev, setTotalRev] = useState(0);


  const loadProfitDetail = async (e) => {
     
    if (isNaN(startDate.getTime())) {
      alert('Invalid Starting Date') ;
      return false;
    } 
    if (isNaN(endDate.getTime())) {
      alert('Invalid Ending Date') ;
      return false;
    } 
    if (endDate < startDate) {
      alert('Ending date must later than Starting date') ;
      return false;
    }
    let difference = endDate.getTime() - startDate.getTime();
    let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
    
    if (TotalDays > 365) {
      alert('Starting and Ending date cannot more than one year');
      return false;
    }

  };
 
  const formatInputStartDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
   //  alert(e.target.value);
    setStartDate(dDate);
  //  setData([]);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
  //  inputRef.current.focus();
  };
  const formatInputeDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setEndDate(dDate);
  //  setData([]);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
  //  inputRef.current.focus();
  };
  const formatInputsDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setStartDate(dDate);
    //   alert(dDate);
    // setTxnDate(dDate);
    //    alert(txnDate);
    // onSearchVoucher(setTxnDate(dDate));
   // inputRef.current.focus();
  };
  const formatInputEndDate = async (e) => {
    e.preventDefault();
    //const cName = e.target.name;
    let dDate = e.target.value;
    // alert(e.target.value);
    setEndDate(dDate);
  
  };
  const onHome = async () => {
    window.location='home';

};

    return (


    
        
      


        <div>
    
          <div className="row">
          
            <div className="col-sm-12" style={{ marginTop: '1px', backgroundColor: '#f3eaef', color: 'black' }}>
              <h2>Income Tax Computation Report</h2>
            </div>
          </div>
     
          <div style={{
      display: 'inline-block',
      width: '1520px',
      height: '50px',
      margin: '6px',
      backgroundColor: 'white',
      border: '4px solid grey',
    }}>
 <label style={{ paddingLeft: "100px", marginTop: '.4rem'}}>
  <a style={{  marginRight: '.8rem' }} >Profit & Loss From Date : </a>
        <input
          type="date"
          maxLength={10}
          value={startDate}
          style={{ width: '10%', border: '1px solid #696969' }}
        //  defaultValue = {sDate}
          onChange={(e) => formatInputsDate(e)}   
          onBlur={(e) => formatInputStartDate(e) }
          name="startDate"
          required
          disabled={false}
          data-tip data-for="sDateTip"
        />
<ReactTooltip id="sDateTip" place="top" effect="solid">
Select Profit & Loss Ending Date which must match COmpany Financial Date
</ReactTooltip>

        <a style={{ marginLeft: '1rem', marginRight: '.8rem' }} >To Date : </a>
                <input
                  type="date"
                  maxLength={10}
                  value={endDate}
            //      defaultValue={eDate}
                  style={{ marginRight: '2rem', width: '10%', border: '1px solid #696969' }}
                  name="endDate"
                 onChange={(e) => formatInputeDate(e)}
                  onBlur={(e) => formatInputEndDate(e) }
                  required
                  disabled={false}
                  data-tip data-for="eDateTip"
                />

         
<ReactTooltip id="eDateTip" place="top" effect="solid">
        Select Profit & Loss Ending Date which must math COmpany Financial Date 
</ReactTooltip>  
<button
            style={{ padding: '4px', marginLeft: '2rem' }}
            type='button'
            class='btn btn-info fa fa-download float-right'
            onClick={() => loadProfitDetail()}
            data-tip data-for="monthTip"
          >Load Tax Computation Detail</button>
<ReactTooltip id="monthTip" place="top" effect="solid">
        Press to generate Profit And Loss Report
</ReactTooltip>
<button
            style={{ padding: '1px', marginLeft: '2rem' }}
            type='button'
            class='btn btn-success'
            onClick={() => onHome()}
            data-tip data-for="homeTip"
          >Home</button>
<ReactTooltip id="homeTip" place="top" effect="solid">
        Press to return Home
</ReactTooltip>
</label>


<span class="square border border-dark"></span>
   <table class="table" style={{ paddingTop: '1px', paddingLeft: '50px', border: '1px solid black' }}>
          <thead class="thead-dark" >
            <tr style={{ align: 'left' }}>          
              <th class="square border border-dark" style={{ backgroundColor: 'white', width: '150px', textAlign: 'center' }}></th>
              <th class="square border border-dark" style={{ backgroundColor: 'white', width: '1000px', textAlign: 'center' }}>REVENUE</th>
              <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}></th>
              <th class="square border border-dark" style={{ backgroundColor: 'white', width: '400px', textAlign: 'center' }}>RM</th>
          
               </tr>
          </thead>
          <tbody style={{align:'left'}} >
            {revData.map(item => {
              return <tr key={item.id}>

                <td class="square border border-dark" style={{ textAlign: 'center', backgroundColor: '#f5f0f0' }}>{item.addNo}</td>
                <td class="square border border-dark" style={{ textAlign: 'left', backgroundColor: '#f5f0f0' }}>{item.glName}</td>
                <td class="square border border-dark" style={{ textAlign: 'right', backgroundColor: '#f5f0f0' }}>{item.totalText}</td>
                <td class="square border border-dark" style={{ textAlign: 'right' }}>{parseFloat(item.amount).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
           
        
              </tr>

            })}
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tbody>
          <tfoot>
           

            <td></td><td></td> 
            <td class="square border border-dark" style={{ textAlign: "center",  backgroundColor: "#eae4e4" }}>TOTAL REVENUE :</td>           
            <td class="square border border-dark" style={{ textAlign: "right",backgroundColor: "#eae4e4" , color: "red" }}>{parseFloat(totalRev).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
     
            <td></td>
            <td></td>
            <td></td>
          </tfoot>
        </table>

</div>
</div>

); // return 
}; //function
export default IncomeTaxComputation;
