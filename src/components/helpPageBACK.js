import React, { useEffect, useState } from 'react';
import Axios from 'axios';
const companyID = localStorage.getItem('companyID');
require('dotenv').config();//
 const url = process.env.REACT_APP_SERVER_URL;

function helpPage() {
  const [data, setData] = useState(null);

  useEffect(() => {
    Axios
    .get(url+`/helpPage`,
      {
       params: {
               companyID: companyID,
              }
      }
    )
    .then(response => response.json())
      .then(data => setData(data))
      .catch(error => console.error(error));
  }, []);

  return (
    <div>
      {/* Render the fetched data */}
      {data ? (
        <ul>
          {data.map(item => (
            <li key={item.id}>{item.name}</li>
          ))}
        </ul>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default helpPage;