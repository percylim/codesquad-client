import React, { Component } from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import Axios from 'axios';
import paginationFactory from 'react-bootstrap-table2-paginator';
//import { Button } from 'react-bootstrap';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
//import { useHistory } from "react-router-dom";
require('dotenv').config();//
//const url = 'http://centralsoft.com.my';
 const url = process.env.REACT_APP_SERVER_URL;


const companyID = localStorage.getItem('companyID');

export class SelectProduct extends Component {




  state = {
    product: [],
    columns: [

      {
      dataField: 'productID',
      text: 'Product ID',
      sort: true,
      headerStyle: { backgroundColor: 'yellow' },
      style: { backgroundColor: 'lightgrey'}
    },
    {
      dataField: 'sku',
      text: 'Product SKU',
      sort: true,
      headerStyle: { backgroundColor: 'lightgreen' }
    },
         {
            dataField: 'barcode',
            text: 'Product Barcode',
            sort: false,
            headerStyle: { backgroundColor: 'yellow' },
            style: { backgroundColor: 'lightgrey'}
         },
          {
            dataField: 'productName',
            text: 'Product Name',
            sort: false,
            headerStyle: { backgroundColor: 'lightgreen' }
          },
          {
            dataField: 'description',
            text: 'Product Description',
            sort: false,
            headerStyle: { backgroundColor: 'yellow' },
            style: { backgroundColor: 'lightgrey'}
          },
          {
            dataField: 'unit',
            text: 'Unit Measurement',
            sort: false ,
            headerStyle: { backgroundColor: 'lightgreen' }
          },
          {
            dataField: 'productImage',
            text: 'Product Image',
            sort: false,
            headerStyle: { backgroundColor: 'yellow' },
            style: { backgroundColor: 'lightgrey'}
          },

           {
            dataField: "edit",
            text: "Edit",
            formatter: (cellContent: string, row: IMyColumnDefinition) => {

                    return <button className="btn btn-primary btn-xs" onClick={() => this.onSelectProduct(row.productID, row.barcode, row.productName, row.unit)}>Select</button>

            },
        },

        ],





  };



  onSelectProduct(productID, barcode, productName,unit,){

   // alert(id);
    localStorage.removeItem('productID');
    localStorage.removeItem('productName');
    localStorage.removeItem('unit');
    localStorage.removeItem('barcode');

    localStorage.setItem('productID',productID);
    localStorage.setItem('productName',productName);
    localStorage.setItem('barcode',barcode);
    localStorage.setItem('unit',unit);

    const { history } = this.props;
    history.goBack();

  //  history.goBack();

}



              componentDidMount() {

                     Axios.get(url+'/productList',
                      {
                       params: {
                           companyID: companyID,
                               }
                      }
                    )
                .then(response => {
                  console.log(response.data);
                  this.setState({
                        product: response.data
                  });
                })
                 // alert(customer);

              }



        render() {




         const pagination = paginationFactory({
          page: 2,
          sizePerPage: 5,
          lastPageText: '>>',
          firstPageText: '<<',
          nextPageText: '>',
          prePageText: '<',
          showTotal: true,
          alwaysShowAllBtns: true,
          onPageChange: function (page, sizePerPage) {
            console.log('page', page);
            console.log('sizePerPage', sizePerPage);
          },
          onSizePerPageChange: function (page, sizePerPage) {
            console.log('page', page);
            console.log('sizePerPage', sizePerPage);
          }
        });

       // const { SearchBar, ClearSearchButton } = Search;







                return (
                  <div className="container">
                  <div class="row" className="hdr">
                  <div class="col-sm-12 btn btn-success" style={{ 'height': "50px"}}>
                  <h4 style={{ 'color': 'black'}}> Select Product </h4>
                   </div>
                    </div>
                  <div  style={{ marginTop: 20 }}>



                  <ToolkitProvider
    keyField="id"
    data={ this.state.product }
    columns={ this.state.columns }
    pagination={ pagination }

  >
    {
      props => (
        <div>
        <span class="square border border-dark"></span>
          <BootstrapTable keyField='id' data={ this.state.product } columns={ this.state.columns }  pagination={ pagination } {...props.baseProps} class="table table-bordered border-dark"/>
          <hr />

        </div>
      )
    }
  </ToolkitProvider>


               <hr />

                </div>

                </div>



                )





        }
};

export default SelectProduct;
