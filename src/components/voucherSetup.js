import React, { useState, useEffect, useRef } from 'react'
import Axios from 'axios';
//import { useHistory } from "react-router-dom";
//import EscapeStr from './mysqlConvertChar';
import './Profile.css';
//import ReactDOM from "react-dom";

//import moment from 'moment';

require('dotenv').config();//
 const url = process.env.REACT_APP_SERVER_URL;
const companyID = localStorage.getItem('companyID');



var jvType = 'JV';
var jvData = [];
var voucherType = [
    {label: 'Journal Voucher',
     value: 'JV',
    },
     {label: 'Payment Voucher',
      value: 'PV',
     },
];



function VoucherSetup() {

    const [voucherNo, setVoucherNo] = useState('');
    const [jvInit, setJvInit] = useState('YYMM');


    const inputReference = useRef(null);


    const body = {
        companyID : companyID,
        jvType : jvType,
        jvInit : jvInit,
        voucherNo : voucherNo,
      };


      const onInputChange = async (e) => {
        e.preventDefault();
         console.log(e.target.value);

     console.log(e.target.name);
      console.log(e.target.value);

  };


      const buttonStyle = {
        color: "black",
        backgroundColor: "yellow",
        padding: "2px 10px 2px 10px",
      };

      useEffect(() => {
       setJvInit('YYMM');



    }, []);

    const loadJvInit= (SuppCust) => {
        Axios
        .get(url+`/loadJvInit`,
          {
           params: {
                   companyID: companyID,
                    jvType : jvType,
                  }
          }
        )
        .then(res => {


           jvData = res.data;

       });
    };

 const handleChangeType= async(e) => {
        jvType = e.target.value;

     //    alert(jvType);
      loadJvInit(jvType);
 };


 const handleChangeInit = async(e) => {
    e.preventDefault();
     // alert(e.target.value);
     setJvInit(e.target.value);


  };



const handleChangeVoucherNo = async(e) => {
  e.preventDefault();

   setVoucherNo(e.target.value);


};






  const handleHome = async() => {



    window.location.href='/home';
  }


const onSave = async () => {
   // e.preventDefault();

};








 return (








<div>
<div className="row" style={{ 'margin': "10px", "paddingLeft": "5px" }}>
<div className="col-sm-12 btn btn-success">
Voucher Setup
 </div>
</div>

<label style={{ paddingLeft: "0px"}}>

Voucher Type :
<select onChange={(e) => handleChangeType(e)}>
 {voucherType.map((item) => (
   <option value={item.value} required> {item.label}</option>
)) }

</select>

</label>

<label style={{paddingLeft: '0px'}}>
Voucher Init Character :
<input
  type="text"
  style={{width: '80px'}}
  name="jvinit"
  class="text-uppercase"
  maxLength={4}
  onChange={(e) => handleChangeInit(e)}
  required
   />

</label>
<label style={{paddingLeft: '0px'}}>
Voucher No. Reset :
<input
  type="number"
  style={{width: '100px'}}
  value={voucherNo}
  name="voucherNo"
  placeholder='0'
  step='1'

  onChange={(e) => handleChangeVoucherNo(e)}
   maxLength={10}
/>

</label>

<h4 style={{color: 'red'}}>NOTE : Voucher Init Character can be any character or digit but recommended using 'year year month month (YYMM)',
<p>e.g. 2205=MAY 2022. Any voucher number can reset either yearly or monthly and will less duplication during verifying
  2022-1 and 2023-1 consider as two different number but if use other character No. cannot reset either monthly or yearly
  and have to continuously the auto running number.
 </p>
  </h4>

 </div>

























    ) // return
  };

export default VoucherSetup;
